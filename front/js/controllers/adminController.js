poll.controller("adminController",['$scope', 'adminService', '$timeout', function($scope, adminService, $timeout){
    $scope.questions = [];
    $scope.form_question = null;
    $scope.form_ip = null;
    $scope.selected_question = null;

    //solve nested scope issue
    $scope.save_form_question = function(form) {
        $scope.form_question = form;
    };
    $scope.save_form_ip = function(form) {
        $scope.form_ip = form;
    };

    // get grid data
    $scope.get_all_questions = function(){
        adminService.get_all_questions().then(function(response){
            //console.log(response);

            if(response.data.status == true){
                $scope.questions = response.data.questions;
                $scope.whitelist = response.data.whitelist;
            }else{
                $scope.set_error_message(response.data);
            }
        });
    };
    $scope.get_all_questions();



    /*----------------QUESTIONS------------------*/
    $scope.add_question = function(){

        $scope.question =  {
            title: "",
            description: "",
            answers: [
                {
                    "title": "",
                    "color": "",
                }
            ]
        };
    };

    $scope.edit_question = function(index){
        //console.log(index);
        $scope.selected_question = index;
        $scope.question =  $scope.questions[index];
    };

    //save to db
    $scope.save_question = function(){
        //console.log($scope);
        if($scope.form_question.$valid){

            adminService.save_question($scope.question).then(function(response){
                if(response.data.status == true){
                    $scope.question = null;
                    $scope.get_all_questions();
                }else{
                    $scope.set_error_message(response.data);
                }
            });
        }else{
            $scope.set_error_message("THE FORM IS NOT VALID");
        }
    };

    $scope.cancel_question = function(){
        $scope.question = null;
        $scope.selected_question = null;
    };

    //
    $scope.set_default = function(index){
        adminService.set_default($scope.questions[index].id).then(function(response){
            if(response.data.status == true){
                $scope.get_all_questions();
            }else{
                $scope.set_error_message(response.data);
            }
        });
    };

    $scope.remove_question = function(index){
        adminService.remove_question($scope.questions[index].id).then(function(response){
            if(response.data.status == true){
                $scope.get_all_questions();
            }else{
                $scope.set_error_message(response.data);
            }
        });
    };
    /*-end---------------QUESTIONS------------------*/


    //ERROR
    $scope.set_error_message = function(message, error){

        if(message.message){
            $scope.error_message = message.message;
        }else{
            $scope.error_message = message;
        }


        $timeout(function(){
            $scope.error_message = null;
        }, 5000);
    };


    /*----------------ANSWERS------------------*/
    $scope.remove_answer = function(index){
        $scope.question.answers.splice(index, 1);
    };

    $scope.add_answer = function(){
        $scope.question.answers.push({
            title: "",
            color: "",
        });
    };
    /*-end---------------ANSWERS------------------*/


    $scope.activeTab = 0;


    /*------------------WHITE LIST--------------------*/
    $scope.add_ip = function(){
        $scope.ip =  {
            ip_address : ""
        };
    };

    $scope.cancel_ip = function(){
        $scope.ip =  null;
    };

    //get from backend
    $scope.insert_my_ip = function(){
        adminService.get_my_ip().then(function(response){

            if(response.data.status == true){
                $scope.ip.ip_address = response.data.ip_address;
            }else{
                $scope.set_error_message(response.data);
            }
        });
    };

    //save to db
    $scope.save_ip = function(){
        if($scope.form_ip.$valid) {
            adminService.save_ip($scope.ip).then(function (response) {
                //console.log(response);
                if (response.data.status == true) {
                    $scope.get_all_questions();
                    $scope.ip = null;
                } else {
                    $scope.set_error_message(response.data);
                }
            });
        }else{
            $scope.set_error_message("THE FORM IS NOT VALID");
        }
    };

    $scope.remove_ip = function(index){
        adminService.remove_ip($scope.whitelist[index].id).then(function(response){
            if(response.data.status == true){
                $scope.get_all_questions();
            }else{
                $scope.set_error_message(response.data);
            }
        });
    };

    /*end ------------------WHITE LIST--------------------*/

}]);