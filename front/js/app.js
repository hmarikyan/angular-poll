var poll = angular.module('poll', [
    'ngRoute',
    'mgcrea.ngStrap',
    'poll_front'
]);

//routing
poll.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/poll', {
        templateUrl: 'front/templates/poll.html',
        controller: 'pollController'
    }).
    when('/admin', {
        templateUrl: 'front/templates/admin.html',
        controller: 'adminController'
    }).
    otherwise({
        redirectTo: '/poll'
    });
}]);

poll.config(['$httpProvider', function($httpProvider) {
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
}]);
