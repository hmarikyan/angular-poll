var poll_front = angular.module('poll_front', [
    'mgcrea.ngStrap'
]);

//directive
poll_front.directive("angularPoll",['pollService', function( pollService){

    return {
        restrict : "AE",

        // BIND FUNCTION
        link : function(scope, el, attrs){
            scope.voted = false;
            scope.error_message = null;
            scope.white = null;

            scope.data = null;
            scope.case = null;


            /*--- CSS CONFIG ----*/
            var width = "100%";
            if(attrs.width)
                var width = attrs.width;

            var height = "auto";
            if(attrs.height)
                var height = attrs.height;

            var font = "16px";
            if(attrs.font)
                var font = attrs.font;


            var poll_wrapper = angular.element(el[0].querySelector('.poll-wrapper'));
            poll_wrapper.css({
                width: width,
                height: height,
                "font-size": font
            });

            /*-end -- CSS CONFIG ----*/
        },

        //DIRECTIVE CONTROLLER
        controller: ['$scope',function($scope){
            // INIT
            pollService.get_question().then(function(response){
                if(response.data.status == true){
                    $scope.question = response.data.question;
                    $scope.percentage = response.data.percentage;
                    $scope.voted = response.data.you_voted;
                    $scope.white = response.data.white;
                }else{
                    $scope.set_error_message(response.data);
                }
            });

            // YOUR MARK
            $scope.vote = function(){
                pollService.vote($scope.case, $scope.question.id).then(function(response){
                    //console.log(response);
                    if(response.data.status == true){
                        $scope.voted = true;
                        $scope.get_statistics();

                    } else{
                        $scope.set_error_message(response.data);
                    }
                });
            };

            //try again whitlist user
            $scope.try_again = function(){
                $scope.voted = false;
            };

            //data for progress bar
            $scope.get_statistics = function(){
                pollService.get_statistics($scope.question.id).then(function(response){
                    if(response.data.status == true){
                        $scope.percentage = response.data.percentage;
                    }else{
                        $scope.set_error_message(response.data);
                    }
                });
            };

            //ERROR
            $scope.set_error_message = function(message){
                if(message.message){
                    $scope.error_message = message.message;
                }else{
                    $scope.error_message = message;
                }
            }

        }],
        scope: {

        },

        // FRONT TEMPLATE
        template: '<div class="poll-wrapper">' +
                        '<div class="error_message" ng-show="error_message">' +
                            '{{ error_message }}' +
                        '</div>' +
                        '<div class="question">' +
                            '{{ question.title }} ' +
                        '</div>' +

                        '<div class="alert-info" ng-show="voted" ng-if="!white">You have already voted !</div> ' +

                        '<div class="alert-success">' +
                            '<a href="" ng-show="voted"  ng-if="white" ng-click="try_again()">Try again! since you are in Whitelist!</a> ' +
                        '</div>' +

                        '<!-- QUESTION-->' +
                        '<div class="quiz" ng-hide="voted" >' +
                            '<ul class="answers">' +
                                '<li class="answer" ng-repeat="answer in question.answers">' +
                                    '<input id="case_{{ answer.id }}" class="radio-poll" type="radio" ng-model="$parent.case" value="{{ answer.id }}"> ' +
                                    '<label for="case_{{ answer.id }}" class="label-poll"> {{ answer.title }} </label> ' +
                                '</li> ' +
                            '</ul> ' +
                            '<div class="save"> ' +
                                '<button ng-disabled="case == null" class="btn btn-primary" type="button" ng-click="vote()"> Vote </button> ' +
                            '</div> ' +
                        '</div>' +

                        '<!-- PERCENT--> ' +
                        '<div class="statictics" ng-show="voted" > ' +
                            '<ul class="answers">' +
                                '<li class="answer" ng-repeat="a in percentage.answers"> ' +
                                    '<div class="progress"> ' +
                                        '<div class="progress-bar" style="background : {{ a.answer.color }}; width: {{ a.percent}}%" role="progressbar">' +
                                        '{{  a.answer.title }} {{ a.percent }}% ' +
                                        '</div> ' +
                                    '</div> ' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                   '</div>'
    };

}]);

//service
poll_front.factory("pollService",['$http', function($http){
    return {

        //initial data from backend
        get_question: function(){
            return $http({
                method: 'GET',
                url: base_url + 'action=get_question'
            });
        },

        // send mark
        vote: function(answer_id, question_id){
            return $http.post(
                base_url + 'action=vote',
                {
                    answer_id: answer_id,
                    question_id: question_id
                }
            );
        },

        // get statistics for this question
        get_statistics: function(question_id){
            return $http({
                method: 'POST',
                data: {
                    question_id: question_id
                },
                url:  base_url +'action=get_statistics'
            });
        }
    }
}]);