poll.factory("adminService",['$http', function($http){
    return {
        //questions
        get_all_questions: function(){
            return $http({
                method: 'GET',
                url: base_url + 'action=get_all_questions'
            });
        },

        save_question: function(question){
            return $http.post(
                base_url + 'action=save_question',
                question
            );
        },

        set_default: function(question_id){
            return $http.post(
                base_url + 'action=set_default',
                {
                    question_id: question_id
                }
            );
        },

        remove_question: function(question_id){
            return $http.post(
                base_url + 'action=remove_question',
                {
                    question_id: question_id
                }
            );
        },

        //ip
        get_my_ip: function(){
            return $http.get(
                base_url + 'action=get_my_ip'
            );
        },

        save_ip: function(ip){
            return $http.post(
                base_url + 'action=save_ip',
                {
                    ip: ip
                }
            );
        },

        remove_ip: function(ip_id){
            return $http.post(
                base_url + 'action=remove_ip',
                {
                    ip_id: ip_id
                }
            );
        }
    }

}]);