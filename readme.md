## INTRODUCTION
PHP/MySql/AngularJS/Twitter.Bootstrap

Poll/quiz with management panel

## REQUIREMENTS
Git
NodeJS/Bower
Apache/PHP/MySql


## INSTALLATION
Clone from repository 
Install composer
Install bower

Config  
back/config.php
front/config.js

Run Migration


## USAGE
Open 
http://localhost/angular-poll
http://localhost/angular-poll/poll_test.html