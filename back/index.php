<?php
error_reporting(E_ALL);

//init application
require_once("init.php");

//models
use back\models\Question;
use back\models\Answer;
use back\models\Vote;
use back\models\Whitelist;

use back\libraries\IPaddress;




/****************************************************************/

try{
    $response = array(
        'status' => true
    );

    if(empty($_GET['action']))
        throw new Exception("No action specified");

    // route
    switch($_GET['action']){

        //GET QUESTION WITH ANSWERS
        case "get_question": {
            $response['question'] = Question::with("answers")
                ->where("is_default", '1')
                ->first();

            if(!$response['question'])
                throw new Exception("No default question");

            $response['white'] = Whitelist::where("ip_address" ,IPaddress::get_ip())->exists();

            //check whitelist
            if(Whitelist::where("ip_address" ,IPaddress::get_ip())->exists()){
                $response['you_voted'] = false;
            }else{
                $response['you_voted'] = Vote::join('questions', 'questions.id', '=', 'votes.question_id')
                    ->where('questions.is_default', '1')
                    ->where("votes.ip_address", IPaddress::get_ip())
                    ->exists();
            }

            $response['percentage'] = Vote::get_percentage($response['question']->id);
            break;
        }


        //VOTE
        case "vote": {

            if(empty($_POST->question_id) || empty($_POST->answer_id))
                throw new Exception("input data is not correct");

            //voted or not
            $v = Vote::where("ip_address", IPaddress::get_ip())
                ->where("question_id", $_POST->question_id)
                ->exists();

            //check whitelist
            if(Whitelist::where("ip_address" ,IPaddress::get_ip())->exists()){

            }elseif($v){
                throw new Exception("You have already voted");
            }

            //insert Vote
            $v = new Vote();
            $v->question_id = $_POST->question_id;
            $v->answer_id = $_POST->answer_id;
            $v->ip_address = IPaddress::get_ip();
            $v->save();

            break;
        }

        //STATISTICS
        case "get_statistics": {
            if(empty($_POST->question_id))
                throw new Exception("input data is not correct");

            $response['percentage'] = Vote::get_percentage($_POST->question_id);
            break;
        }

        //STATISTICS
        case "get_all_questions": {
            $response['questions'] = Question::with("answers")->get();
            foreach($response['questions'] as $q){
                $q['percentage'] = Vote::get_percentage($q->id);
            }

            $response['whitelist'] = Whitelist::all();

            break;
        }

        //DELETE question with answers
        case "remove_question": {

            if(empty($_POST->question_id))
                throw new Exception("input data is not correct");

            Question::where("id", $_POST->question_id)->delete();
            Answer::where("question_id", $_POST->question_id)->delete();

            //$response['questions'] = Question::with("answers")->get();
            break;
        }

        //
        case "save_question": {

            //question
            if(!empty($_POST->id)) {
                $q = Question::firstOrNew(array('id' => $_POST->id));
            }else{
                $q = new Question();
            }
            $q->title = $_POST->title;
            $q->description = $_POST->description;

            if(!empty( $_POST->is_default))
                $q->is_default = $_POST->is_default;

            $q->save();

            //answers
            $a_ids = array();
            if(!empty($_POST->answers)){
                foreach($_POST->answers as $_a){
                    if(!empty($_a->id)) {
                        $a = Answer::firstOrNew(array('id' => $_a->id));
                    }else{
                        $a = new Answer();
                    }

                    $a->title = $_a->title;
                    $a->color = $_a->color;
                    $a->question_id = $q->id;
                    $a->save();

                    $a_ids[] = $a->id;
                }

                //remove remaining answers
                Answer::where("question_id", $a->question_id)
                    ->whereNotIn('id', $a_ids)
                    ->delete();
            }

            break;
        }

        //SET DEFAULT QUESTION
        case "set_default": {

            if(empty($_POST->question_id)) {
                throw new Exception("input data is not correct");
            }

            Question::where("is_default", 1)->update(array('is_default' => 0));

            Question::where("id", $_POST->question_id)->update(array('is_default' => 1));

            break;
        }

        //DELETE question with answers
        case "get_my_ip": {

            $response["ip_address"] = IPaddress::get_ip();
            break;
        }

        //save ip address to whitlist
        case "save_ip": {
            //question
            if(empty($_POST->ip))
                throw new Exception("input data is not correct");

            $w = Whitelist::where(array('ip_address' => $_POST->ip->ip_address))->exists();
            if(!empty($w))
                throw new Exception("This IP address already exists");

            $w = new Whitelist();
            $w->ip_address = $_POST->ip->ip_address;
            $w->save();

            break;
        }

        //delete ip
        case "remove_ip": {
            if(empty($_POST->ip_id))
                throw new Exception("input data is not correct");

            Whitelist::where("id", $_POST->ip_id)->delete();
            break;
        }

        default: {
            throw new Exception("No handler for this action");
            break;
        }
    }

}catch(Exception $e){
    $response = array(
        'status' => false,
        'message' => $e->getMessage()
    );
}
//response
finally{
    //print_r($response);
    echo json_encode($response);
    die;
}
?>