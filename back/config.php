<?php

return array(
    'db' => array(
        'host'      => 'localhost',
        'database'  => 'poll',
        'username'  => 'root',
        'password'  => '',
        'prefix'    => 'poll_',
    ),
);