<?php

require_once("init.php");

use Illuminate\Database\Capsule\Manager as DB;

use back\models\Question;
use back\models\Answer;
use back\models\Vote;
use back\models\Whitelist;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class MigrateCommand
 */
class MigrateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('migrate')
            ->setDescription('Migrate poll tables')
            ->addArgument(
                'direction',
                InputArgument::OPTIONAL,
                'Up or Down'
            );
    }

    //main function
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dir = $input->getArgument('direction');

        if (!$dir){
            $this->up($output);
        }elseif($dir == "up") {
            $this->up($output);
        } else {
            $this->down($output);
        }
    }

    //MIGRATE UP
    public function up($output){
        $output->writeln("Migrating...");

        $engine = 'InnoDB';

        DB::schema()->create('questions', function($table)  use ($engine)
        {
            $table->engine = $engine;
            $table->increments('id');
            $table->string('title', 100)->comment = "Question";
            $table->text('description')->default("")->comment = "";
            $table->integer('is_default')->default("0")->comment = "Default question";
        });
        $output->writeln("success: table questions");

        DB::schema()->create('answers', function ($table) use ($engine)
        {
            $table->engine = $engine;

            $table->increments('id');
            $table->integer('question_id')->comment = "Refers to Question";
            $table->string('title', 100)->comment = "Answer case";
            $table->string('color', 30)->default("green")->comment = "Color for porgress bar";

            //$table->foreign('question_id')->references('id')->on('questions');
        });
        $output->writeln("success: table answers");


        DB::schema()->create('votes', function($table) use ($engine)
        {
            $table->engine = $engine;
            $table->increments('id');
            $table->integer('question_id')->comment = "Refers to Questions";
            $table->integer('answer_id')->comment = "Refers to Answers";
            $table->string('ip_address', 30)->comment = "client IP address";

            //$table->foreign('question_id')->references('id')->on('questions');
            //$table->foreign('answer_id')->references('id')->on('answers');
        });
        $output->writeln("success: table votes");


        DB::schema()->create('whitelist', function($table) use ($engine)
        {
            $table->engine = $engine;
            $table->increments('id');
            $table->string('ip_address', 30)->comment = "client's IP address";
        });
        $output->writeln("success: table whitelist");

        $output->writeln("inserting test data");
        $this->insert_data();

        $output->writeln("Finish !");
    }


    // TEST DATA for application
    public function insert_data(){

        $q = new Question();
        $q->title = "How are you?";
        $q->description = "Random description";
        $q->is_default = "1";
        $q->save();

        $a = new Answer();
        $a->title = "Fine";
        $a->color = "green";
        $q->answers()->save($a);

        $a = new Answer();
        $a->title = "Good";
        $a->color = "blue";
        $q->answers()->save($a);

        $a = new Answer();
        $a->title = "Soso";
        $a->color = "brown";
        $q->answers()->save($a);

        $a = new Answer();
        $a->title = "Bad";
        $a->color = "red";
        $q->answers()->save($a);

    }

    //MIGRATE DOWN
    public function down($output){
        $output->writeln("Downgrading...");

        DB::schema()->drop('questions');
        DB::schema()->drop('answers');
        DB::schema()->drop('votes');
        DB::schema()->drop('whitelist');

        $output->writeln("Finish !");
    }
}


$application = new Application();
$application->add(new MigrateCommand());
$application->run();