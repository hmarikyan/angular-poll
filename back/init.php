<?php

require_once(__DIR__ ."/../vendor/autoload.php");
$config = require_once(__DIR__ . "/config.php");

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB;
$db->addConnection([
    'driver'    => 'mysql',
    'host'      => $config['db']['host'],
    'database'  => $config['db']['database'],
    'username'  => $config['db']['username'],
    'password'  => $config['db']['password'],
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => $config['db']['prefix'],
]);

// Make this Capsule instance available globally via static methods... (optional)
$db->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$db->bootEloquent();


//obtain post data
$_POST = json_decode(file_get_contents("php://input"));