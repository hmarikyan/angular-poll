<?php

namespace back\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 * @package back\models
 */
class Answer extends Model {

    public $timestamps = false;

    //relationship
    public function question()
    {
        return $this->belongsTo('back\models\Question', 'question_id');
    }

    //relationship
    public function votes()
    {
        return $this->hasMany('back\models\Vote', 'answer_id');
    }
}

