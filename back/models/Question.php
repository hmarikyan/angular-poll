<?php
namespace back\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 * @package back\models
 */
class Question extends Model {

    public $timestamps = false;

    //relationship
    public function answers()
    {
        return $this->hasMany('back\models\Answer', 'question_id');
    }

    //relationship
    public function votes()
    {
        return $this->hasMany('back\models\Vote', 'question_id');
    }

}


