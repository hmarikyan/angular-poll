<?php

namespace back\models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Vote
 * @package back\models
 */
class Vote extends Model {

    public $timestamps = false;

    //relationship
    public function question()
    {
        return $this->belongsTo('back\models\Question', 'question_id');
    }

    //relationship
    public function answer()
    {
        return $this->belongsTo('back\models\Answer', 'answer_id');
    }

    // GET PERCENTAGE
    public static function get_percentage($question_id){
        $all_votes_count = static::where("question_id",$question_id)->count();

        if(!$all_votes_count)
            return false;

        $all_answers = Answer::where("question_id",$question_id)->get();

        $percent = array(
            "question" => Question::find($question_id),
            "answers" => array()
        );

        //percent for each answer
        foreach($all_answers as $a){
            $a_votes_count = static::where("question_id",$question_id)
                ->where("answer_id", $a->id)
                ->count();
            $p = round($a_votes_count / $all_votes_count * 100);

            $percent["answers"][] = array(
                "answer" => $a,
                "percent" => $p
            );
        }

        return $percent;
    }
}

