<?php

namespace back\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Whitelist
 * @package back\models
 */
class Whitelist extends Model {

    protected $table = 'whitelist';
    public $timestamps = false;
}

